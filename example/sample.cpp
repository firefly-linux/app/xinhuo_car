/*********************************************************************************************************************************
*一、示例程序运行逻辑：
*	从命令行读取检测识别图片
*		-调用车量检测接口
*		-如果检测到车辆
*			*把每个车量信息放入车牌检测接口（PlateDetect_withVD），车牌识别接口进行车牌检测与识别，把结果打印出来并显示至
* 			 图片(result.bmp)上
*		-如果没有检测到车辆 
*			*创建车牌信息结构体指针（PlateInfo*），调用车牌检测接口（PlateDetect）,车牌识别接口，把结果打印出来并显示至图
*			 片(result.bmp)上
*
*二、编译示例程序：
*	编译sample.cpp
*		./build.sh sample
*	也可编译您自己写的程序，如上格式编译就行
*	注意：编译前请编辑build.sh脚本文件，替换您自己的交叉编译工具位置，并赋予可自行权限
*
*三、运行方式：
*	运行run.sh，直接运行测试程序，传入准备好的测试图片，并显示结果
*	如果您想检测识别其他图片，如下运行即可
*   	./sample ./image/test.jpg
*	注意：运行示例程序前请设置动态链接库文件位置（LD_LIBRARY_PATH），并赋予可自行文件与脚本可自行权限
*********************************************************************************************************************************/
#include <sys/time.h>
#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>

#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

#include "xhlpr_api.h"
#include "xhlpr_type.h"


int main(int argc, char *argv[]) {
	//查看SDK版本
	float version;
	XHLPRAPI_Version(&version);
	
	//读取激活码，并打印激活码
	std::ifstream activation_file;
	activation_file.open("../licSever/activation.conf");
	if (!activation_file)
	{
		std::cout << "activation.conf not found" << std::endl;
		return -1;
	}
	std::string activation_code;
	getline(activation_file, activation_code);
	activation_file.close();
	std::cout << "activation_code: " << (char*)activation_code.data() << std::endl;
	
	//程序开始调用初始化接口初始化激活
	int ret = XHLPRInit("../licSever", activation_code.c_str());
	if (ret != LPR_OK) {
		std::cout << "XHLPRInit ERROR:" << ret << std::endl;
		return ret;
	}
	
	//新建车辆检测会话
	XHLPR_SESS sess_vd;
	ret = VDCreate(&sess_vd);
	if (ret != LPR_OK) {
		std::cout << "VDCreate ERROR:" << ret << std::endl;
		return ret;
	}
	
	//新建车牌检测会话
	XHLPR_SESS sess_pd;
	ret = PDCreate(&sess_pd);
	if (ret != LPR_OK) {
		std::cout << "PDCreate ERROR:" << ret << std::endl;
		return ret;
	}
	
	//新建车牌识别会话
	XHLPR_SESS sess_pr;
	ret = PRCreate(&sess_pr);
	if (ret != LPR_OK) {
		std::cout << "PRCreate ERROR:" << ret << std::endl;
		return ret;
	}
	
	
	struct timeval start, end;
	double runtime_vd,runtime_pd,runtime_pr;
	

	cv::Mat img = cv::imread(argv[1]);
	cv::Mat img_clone = img.clone();
	
	//创建车辆集结构体
	Vehicles vehicle_list;
	vehicle_list.count = 0;
	
	//调用车辆检测接口
	gettimeofday(&start, NULL);
	ret = VehicleDetect(sess_vd,img.data, img.cols, img.rows, &vehicle_list);
	gettimeofday(&end, NULL);
	runtime_vd = int(((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec)) / 1000 + 0.5);
	if (ret != LPR_OK) {
		std::cout << "VehicleDetect ERROR:" << ret << std::endl;
		return ret;
	}
	
	//打印车辆运行时间与识别车辆的个数
	std::cout<<"VehicleDetect runtime:"<<runtime_vd<<"ms"<<std::endl;
	std::cout<<"Number of vehicles:"<<vehicle_list.count<<std::endl;
	std::cout<<std::endl;
	
	for (int i = 0; i < vehicle_list.count; i++) {
		int count = -1;
		//调用车牌检测接口，PlateDetect_withVD接口在检测到车辆时运行
		gettimeofday(&start, NULL);
		ret = PlateDetect_withVD(sess_pd, img.data, img.cols, img.rows, &(vehicle_list.vehicle[i]), &count);
		gettimeofday(&end, NULL);
		if (ret != LPR_OK) {
			std::cout << "PlateDetect_withVD ERROR:" << ret << std::endl;
			return ret;
		}
		runtime_pd = int(((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec)) / 1000 + 0.5);

		//打印车辆坐标与车牌运行时间
		std::cout << "----------------------------------------------------" << std::endl;
		std::cout<<"Vehicle local:("
		<<vehicle_list.vehicle[i].vehicle_rect.x<<","<<vehicle_list.vehicle[i].vehicle_rect.y<<","
		<<vehicle_list.vehicle[i].vehicle_rect.width<<","<<vehicle_list.vehicle[i].vehicle_rect.height<<")"
		<<" score:" << vehicle_list.vehicle[i].score<<std::endl;
		std::cout<<"PlateDetect_withVD runtime:"<<runtime_pd<<"ms"<<std::endl;
		
		if(count>0){
			//调用车牌识别接口
			gettimeofday(&start, NULL);
			ret = PlateOCR(sess_pr, img.data, img.cols, img.rows, &(vehicle_list.vehicle[i].plate));
			gettimeofday(&end, NULL);
			if (ret != LPR_OK) {
				std::cout << "PlateOCR ERROR:" << ret << std::endl;
				return ret;
			}
			runtime_pr = int(((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec)) / 1000 + 0.5);
			
			//打印车牌坐标、车牌识别接口运行时间、车牌号、车牌识别置信度、车牌类型返回码与车牌颜色返回码
			std::cout<<"License Plate local:("
			<<vehicle_list.vehicle[i].plate.points[0].x<<","<<vehicle_list.vehicle[i].plate.points[0].y<<")("
			<<vehicle_list.vehicle[i].plate.points[1].x<<","<<vehicle_list.vehicle[i].plate.points[1].y<<")("
			<<vehicle_list.vehicle[i].plate.points[2].x<<","<<vehicle_list.vehicle[i].plate.points[2].y<<")("
			<<vehicle_list.vehicle[i].plate.points[3].x<<","<<vehicle_list.vehicle[i].plate.points[3].y<<")"
			<<" score:"<<vehicle_list.vehicle[i].plate.points_score<<std::endl;
			std::cout<<"PlateOCR runtime:"<<runtime_pr<<"ms"<<std::endl;
			if(vehicle_list.vehicle[i].plate.number_score > 0){
				std::cout<<"Plate Number:"<<vehicle_list.vehicle[i].plate.plateNumber<<"  score:"<<vehicle_list.vehicle[i].plate.number_score<<std::endl;
				std::cout<<"type:"<<vehicle_list.vehicle[i].plate.type<<"  color:"<<vehicle_list.vehicle[i].plate.color<<std::endl;
			}else{
				std::cout<<"License plate recognition failure!!!"<<std::endl;
			}
			
			//在图片中显示车牌位置与置信度
			cv::rectangle(img_clone, cv::Point(vehicle_list.vehicle[i].plate.points[0].x, vehicle_list.vehicle[i].plate.points[0].y-20), cv::Point(vehicle_list.vehicle[i].plate.points[0].x+80, vehicle_list.vehicle[i].plate.points[0].y), cv::Scalar(0, 0, 255), -1, 1, 0);
			cv::putText(img_clone, std::to_string(vehicle_list.vehicle[i].plate.points_score), cv::Point(vehicle_list.vehicle[i].plate.points[0].x, vehicle_list.vehicle[i].plate.points[0].y-4), 1, 1, CV_RGB(255, 255, 255));			
			for(int j = 0;j < 4; j++){
				cv::line(img_clone,cv::Point(vehicle_list.vehicle[i].plate.points[j].x, vehicle_list.vehicle[i].plate.points[j].y), cv::Point(vehicle_list.vehicle[i].plate.points[(j+1)%4].x, vehicle_list.vehicle[i].plate.points[(j+1)%4].y), cv::Scalar(0, 0, 255), 1);
			}
		}else{
			std::cout<<"No license plate detected!!!"<<std::endl;
		}
		std::cout << "----------------------------------------------------" << std::endl;
		
		//在图片中显示车量位置与置信度
		cv::rectangle(img_clone, cv::Point(vehicle_list.vehicle[i].vehicle_rect.x, vehicle_list.vehicle[i].vehicle_rect.y-20), cv::Point(vehicle_list.vehicle[i].vehicle_rect.x+80, vehicle_list.vehicle[i].vehicle_rect.y), cv::Scalar(255, 0, 0), -1, 1, 0);
		cv::putText(img_clone, std::to_string(vehicle_list.vehicle[i].score), cv::Point(vehicle_list.vehicle[i].vehicle_rect.x, vehicle_list.vehicle[i].vehicle_rect.y-4), 1, 1, CV_RGB(255, 255, 255));
		cv::Rect rect(vehicle_list.vehicle[i].vehicle_rect.x, vehicle_list.vehicle[i].vehicle_rect.y, vehicle_list.vehicle[i].vehicle_rect.width,vehicle_list.vehicle[i].vehicle_rect.height);
		cv::rectangle(img_clone, rect,cv::Scalar(255, 0, 0), 1, 8, 0);
	}
	//如果没有检测到车辆、直接调用车牌检测接口，
	if(vehicle_list.count == 0){
		PlateInfo *plate;
		int count = -1;
		
		//调用车牌检测接口
		gettimeofday(&start, NULL);
		ret = PlateDetect(sess_pd, img.data, img.cols, img.rows, &plate, &count);
		gettimeofday(&end, NULL);
		if (ret != LPR_OK)
		{
			std::cout << "PlateDetect Error: " << ret << std::endl;
			return -1;
		}

		//打印车牌检测运行时间与车牌数量
		runtime_pd = int(((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec)) / 1000 + 0.5);
		std::cout<<"PlateDetect runtime:"<<runtime_pd<<"ms"<<std::endl;
		std::cout<<"Number of license plate:"<<count<<std::endl<<std::endl;
		
		for(int j = 0; j < count;j++){
			//运行车牌识别接口
			gettimeofday(&start, NULL);
			ret = PlateOCR(sess_pr, img.data, img.cols, img.rows, plate+j);
			gettimeofday(&end, NULL);
			if (ret != LPR_OK)
			{
				std::cout << "PlateOCR Error: " << ret << std::endl;
				return -1;
			}
			runtime_pr = int(((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec)) / 1000 + 0.5);
			
			//打印车牌坐标、车牌识别接口运行时间、车牌号、车牌识别置信度、车牌类型返回码与车牌颜色返回码
			std::cout << "----------------------------------------------------" << std::endl;
			std::cout<<"PlateOCR runtime:"<<runtime_pr<<"ms"<<std::endl;
			std::cout<<"License Plate local:("
			<<plate[j].points[0].x<<","<<plate[j].points[0].y<<")("
			<<plate[j].points[1].x<<","<<plate[j].points[1].y<<")("
			<<plate[j].points[2].x<<","<<plate[j].points[2].y<<")("
			<<plate[j].points[3].x<<","<<plate[j].points[3].y<<")"
			<<" score:"<<plate[j].points_score<<std::endl;
			
			if(plate[j].number_score > 0){
				std::cout<<"Plate Number:"<<plate[j].plateNumber<<"  score:"<<plate[j].number_score<<std::endl;
				std::cout<<"type:"<<plate[j].type<<"  color:"<<plate[j].color<<std::endl;
			}else{
				std::cout<<"License plate recognition failure!!!"<<std::endl;
			}
			std::cout << "----------------------------------------------------\n" << std::endl;
			
			
			//在图片中显示车牌位置与置信度
			cv::rectangle(img_clone, cv::Point(plate[j].points[0].x, plate[j].points[0].y-20), cv::Point(plate[j].points[0].x+80, plate[j].points[0].y), cv::Scalar(0, 0, 255), -1, 1, 0);
			cv::putText(img_clone, std::to_string(plate[j].points_score), cv::Point(plate[j].points[0].x, plate[j].points[0].y-4), 1, 1, CV_RGB(255, 255, 255));
			for(int k = 0;k < 4; k++){
				cv::line(img_clone,cv::Point(plate[j].points[k].x, plate[j].points[k].y), cv::Point(plate[j].points[(k+1)%4].x, plate[j].points[(k+1)%4].y), cv::Scalar(0, 0, 255), 1);
			}
		
		}
		if(count <= 0){
			std::cout<<"No license plate detected!!!"<<std::endl;
		}
		
	}

	cv::imwrite("result.bmp",img_clone);
	
	
	//调用车辆检测会话销毁接口
	ret = VDDestroy(&sess_vd);
	if (ret != LPR_OK) {
		std::cout << "VDDestroy ERROR:" << ret << std::endl;
		return ret;
	}
	
	//调用车牌检测会话销毁接口
	ret = PDDestroy(&sess_pd);
	if (ret != LPR_OK) {
		std::cout << "PDDestroy ERROR:" << ret << std::endl;
		return ret;
	}

	//调用车牌识别会话销毁接口
	ret = PRDestroy(&sess_pr);
	if (ret != LPR_OK) {
		std::cout << "PRDestroy ERROR:" << ret << std::endl;
		return ret;
	}
	
	//程序结束，调用逆初始化接口
	XHLPRFinal();
	return 0;
}