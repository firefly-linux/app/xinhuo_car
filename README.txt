尊敬的客户，使用我们提供的接口需要联网激活，请确保您的测试设备连接互联网。
以下是文件夹描述：
doc：API文档文件夹
example：命令行Demo所在文件夹
include：存放编译所需要的头文件
lib：存放编译运行所需要的的so文件
licSever：存放配置文件与临时授权所需文件
注意：运行我们的Demo前，请将我们提供的激活码替换licSever/activation.conf文件中的sparkAI_lprtest
